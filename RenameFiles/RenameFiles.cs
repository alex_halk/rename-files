﻿using System;
using System.IO;

namespace RenameFiles
{
    public class RenameFiles
    {
        public static void RenameFileNames(string directoryWithFiles)
        {
            DirectoryInfo directory = new DirectoryInfo(directoryWithFiles);

            var files = directory.GetFiles();

            foreach (var file in files)
            {
                string newName = Path.GetFileNameWithoutExtension(file.FullName) + "-" + DateTime.Now.ToString("dd-MM-yyyy hh-mm-ss") + file.Extension;

                file.MoveTo(directoryWithFiles + "\\" + newName);
            }

        }

        public static void RenameFileNamesMain(string pathFolder)
        {
            try
            {
                var currentSubFolders = Directory.GetDirectories(pathFolder);

                if (currentSubFolders.Length > 0)
                {
                    foreach (var item in currentSubFolders)
                    {
                        RenameFileNamesMain(item);
                    }
                    RenameFileNames(pathFolder);
                }
                else if (Directory.GetFiles(pathFolder).Length > 0) RenameFileNames(pathFolder);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("incorect path of directory");
            }

        }
    }
}
