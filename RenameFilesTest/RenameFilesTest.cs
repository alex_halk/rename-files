﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RenameFilesTest
{
    [TestClass]
    public class RenameFilesTest
    {
        List<string> listWithFile = new List<string>();//For search all files used by recursion

        /*// First version of first method. Is checking has names of file changed.
        [TestMethod]
        public void IsRenamedFile()
        {
            //Arrange
            string newFolder = "testingDirectory";
            Directory.CreateDirectory(newFolder);

            var pathWithFiles = Directory.GetCurrentDirectory() + "\\" + newFolder + "\\";
            for (int i = 0; i < 5; i++)
            {
                File.Create(pathWithFiles + "file" + i + ".txt").Close();
            }
            var listOfFilesInDirectory = new List<string>(Directory.GetFiles(pathWithFiles));

            //Act
            RenameFiles.RenameFiles.RenameFileNames(pathWithFiles);

            //Assert
            Assert.AreNotEqual(listOfFilesInDirectory, Directory.GetFiles(pathWithFiles));
        }
        */

        //First Method. Is checking has file renamed, also in subfolders.
        [TestMethod]
        public void IsRenamedIcludingFiles()
        {
            CreateTestingFoldersAndFile(Directory.GetCurrentDirectory());

           // string path = Directory.GetCurrentDirectory() + "\\" + "testingDirectory2";
           string path = CreateTestingFoldersAndFile(Directory.GetCurrentDirectory());
            GetListOfNameWithAllFile(path);
            var listOldName = new List<string>(listWithFile);

            RenameFiles.RenameFiles.RenameFileNamesMain(path);

            listWithFile.Clear();
            GetListOfNameWithAllFile(path);

            var listNewNames = new List<string>(listWithFile);

            Assert.AreNotEqual(listOldName, listNewNames);

        }

        //Second Method. Check is new name valid for my task.(Must have curent date and time)
        [TestMethod]
        public void IsRenamedNameValid()
        {

            //Arrange
            string changedParts = null;
            string newFolder = "testingDirectory1";
            bool result = false;
            if (Directory.Exists(newFolder))
            {
                Directory.Delete(newFolder, true);
                Directory.CreateDirectory(newFolder);
            }
            else Directory.CreateDirectory(newFolder);

            var pathWithFile = Directory.GetCurrentDirectory() + "\\" + newFolder + "\\";

            string name = "validName";
            File.Create(pathWithFile + name + ".txt").Close();

            //Act
            RenameFiles.RenameFiles.RenameFileNames(pathWithFile);

            var nameOfFile = Directory.GetFiles(pathWithFile);
            if (nameOfFile.Length == 1)
            {
                Assert.IsTrue(ParseNewName(nameOfFile[0], name));
            }
            else Assert.Fail();

        }


        //Helper method for parse new name. Using in second method.
        private bool ParseNewName(string nameOfFile, string previouslyName)
        {
            var changedParts = Path.GetFileNameWithoutExtension(nameOfFile).Substring(previouslyName.Length + 2);
            var separatedName = changedParts.Split(' ');

            if (separatedName.Length > 2) return false;
            else if (separatedName.Length == 2)
            {
                separatedName[0] = separatedName[0].Replace('-', '.');
                separatedName[1] = separatedName[1].Replace('-', ':');
            }

            try
            {
                var asd = Convert.ToDateTime(separatedName[0] + " " + separatedName[1]);
                return true;
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        //Helper method for get all files in path, include subfolders. Using in first method.
        private void GetListOfNameWithAllFile(string path)
        {
            listWithFile = new List<string>();

            var includeDirectory = Directory.GetDirectories(path);
            if (includeDirectory.Length > 0)
            {
                foreach (var item in includeDirectory)
                {
                    GetListOfNameWithAllFile(item);
                }
                listWithFile.AddRange(Directory.GetFiles(path));
            }
            else
            {
                listWithFile.AddRange(Directory.GetFiles(path));
            }
        }

        //Create testing folders
        private string CreateTestingFoldersAndFile(string path)
        {
            List<string> listWithFolders = new List<string>()
            {
                path + "\\TestingFolders" + "\\Test Folder" + 0 + "\\SecondLevel" + 0,
                path + "\\TestingFolders" + "\\Test Folder" + 1 + "\\SecondLevel" + 1+"\\ThirdLevel" + 1,

                path + "\\TestingFolders" + "\\Test Folder" + 2 + "\\SecondLevel" + 2
            };


            foreach (var item in listWithFolders)
            {
                Directory.CreateDirectory(item);
                File.Create(item + "\\" + "test1.txt").Close();
                File.Create(item + "\\" + "example2.txt").Close();
                File.Create(item + "\\" + "some3.txt").Close();
            }

            return path + "\\TestingFolders";
        }
    }
}
